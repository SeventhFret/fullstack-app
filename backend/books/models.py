from django.db import models

class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.URLField(default="https://picsum.photos/500.jpg")
