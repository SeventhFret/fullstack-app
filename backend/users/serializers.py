from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.core.exceptions import ValidationError
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, Serializer

class UserRegistrationSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['email', 'username', 'first_name', 'last_name', 'password']
    
    def create(self, clean_data):
        user_object = User.objects.create_user(username=clean_data["username"],
                                               email=clean_data["email"],
                                               first_name=clean_data["first_name"],
                                               last_name=clean_data["last_name"],
                                               password=clean_data["password"])
        user_object.save()
        return user_object
    
    
class UserLoginSerializer(Serializer):
    username = serializers.CharField()
    password = serializers.CharField()
    
    def check_user(self, clean_data):
        user = authenticate(username=clean_data['username'], password=clean_data['password'])
        
        if not user:
            raise ValidationError("User not found")
        return user
    
    
class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', "email"]