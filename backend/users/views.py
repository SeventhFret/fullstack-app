from django.contrib.auth.models import User
from django.contrib.auth import login, logout
from django.core.exceptions import ValidationError
from rest_framework.authentication import SessionAuthentication
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions
from rest_framework.authtoken.models import Token
from .serializers import UserRegistrationSerializer, UserLoginSerializer, UserSerializer


def flatten_errors(errors: dict):
    all_errors = []
    
    for field in errors.keys():
        all_errors.extend(errors[field])
    
    return all_errors
    

class LoginView(APIView):
    permission_classes = [permissions.AllowAny]
    authentication_classes = [SessionAuthentication]
    
    def post(self, request):
        data = request.data
        
        serializer = UserLoginSerializer(data=data)
        
        if serializer.is_valid():
            try:
                user = serializer.check_user(data)
            except ValidationError:
                return Response({"errors": ["Wrong credentials"]}, status.HTTP_404_NOT_FOUND)            
                
            token = Token.objects.filter(user=user).first()
            
            return Response({"token": token.key}, status.HTTP_200_OK)
        
        flattened_errors = flatten_errors(serializer.errors)
        
        return Response({"errors": flattened_errors}, status.HTTP_400_BAD_REQUEST)    
    
    
class RegistrationView(APIView):
    permission_classes = [permissions.AllowAny]
    
    def post(self, request):
        data = request.data
        if User.objects.filter(email=data['email']).exists():
            return Response({"errors": ["This email is already taken"]}, status.HTTP_400_BAD_REQUEST)
        
        # TODO: Create decryption for encrypted user data
        
        serializer = UserRegistrationSerializer(data=request.data)
        
        if serializer.is_valid():
            user = serializer.create(data)
            
            if user:
                token = Token.objects.create(user=user)
                token.save()
                
                return Response({"messages": ["Registered successfully!"]}, status.HTTP_201_CREATED)
            
        flattened_errors = flatten_errors(serializer.errors)
        
        return Response({"errors": flattened_errors}, status.HTTP_400_BAD_REQUEST)
        
    

class LogoutView(APIView):
    

    def post(self, request):
        logout(request)
        return Response({"message": "Logged out."}, status.HTTP_200_OK)

