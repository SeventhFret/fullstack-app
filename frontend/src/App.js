import './App.css';
import { Routes, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { RegisterForm } from './pages/Register';
import { LoginForm } from './pages/Login';
import { Logout } from './pages/Logout';
import { BooksList } from './pages/Books';
// import { useState, useEffect } from 'react';
// import axios from 'axios';

// axios.defaults.xsrfCookieName = 'csrftoken';
// axios.defaults.xsrfHeaderName = 'X-CSRFToken';
// axios.defaults.withCredentials = true;

function App() {
  const token = localStorage.getItem('token');
  let loggedIn = false;

  if (token) {
    loggedIn = true;
  } else {
    loggedIn = false;
    
  }


  return (

    <Routes>
      <Route path="/" element={<Home loggedIn={loggedIn} />} />
      <Route path="registration/" element={<RegisterForm loggedIn={loggedIn} />} />
      <Route path='login/' element={<LoginForm loggedIn={loggedIn} />} />
      <Route path='logout/' element={<Logout />} />
      <Route path='books/' element={<BooksList loggedIn={loggedIn} />} />
    </Routes>
    
  );
}

export default App;
