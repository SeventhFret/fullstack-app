import './Books.css';
import './Home.css';
import { client } from './Login';
import { CreateAppBar } from "../components/NavBar";
import { useState, useEffect } from 'react';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import Alert from '@mui/material/Alert';



export function NotAllowed({loggedIn}) {
    return (
        <div className='Main-section'>
            <CreateAppBar loggedIn={loggedIn} />
            <Typography variant='h2'>You must register or log in to access this resource.</Typography>
    
        </div>
    )
}

export function BooksList({ loggedIn }) {

    if (!loggedIn) {
        <NotAllowed loggedIn={loggedIn} />
    }

    const [booksList, setBooksList] = useState([]);
    const [errors, setErrors] = useState([]);

    const token = localStorage.getItem('token');

    useEffect(() => {
        client.get('/books/', {
            headers: {
                "Authorization": "Token ".concat(token)
            }
        }).then((response) => {
            setBooksList(response.data);
        }).catch((error) => {
                console.log(error);
                setErrors(error);
        })
    }, [token])

        

    return (
        <div className='Books-section'>
            <CreateAppBar loggedIn={loggedIn} />
            <Box sx={{ mx: 5, mt: 10 }}>
                <Typography sx={{ textAlign: 'left' }} variant='h2'>Library book list</Typography>
                { errors.map((error) => (
                    <Alert severity='error'>{error}</Alert>
                )) }
                <Box>
                    <Grid container>
                    { booksList.map((book, index) => (
                        <Grid >
                            <Card sx={{ maxWidth: 345,
                                        m: 3,
                                        transition: 'transform 0.5s',
                                        "&:hover": {
                                            transform: "scale(1.03)",
                                        },
                                }}>
                                <CardMedia
                                sx={{ p: 0, objectFit: 'contain' }}
                                component='img'
                                src={book['image']}
                                >

                                </CardMedia>

                                <CardContent>
                                    <Typography variant='h5'>{book['title']}</Typography>
                                    <Typography variant='body2'>{book['description']}</Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    ))}
                    </Grid>
                </Box>
            </Box>
        </div>
    )

}

