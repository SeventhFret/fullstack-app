import './Home.css';
import { CreateAppBar } from '../components/NavBar';
import Alert from '@mui/material/Alert';


export function Logout() {
    // localStorage.removeItem('token');
    
    return (
        <div className='Main-section'>
            <CreateAppBar loggedIn={false} />
            <Alert sx={{ transform: 'scale(1.5)' }} severity='success'>You was successfully logged out.</Alert>
        </div>
    )
}