import './Home.css';
import { CreateAppBar } from '../components/NavBar';
import Card from '@mui/material/Card';

export function Home({loggedIn}) {
    return (
    <div className="Main-section">
      <CreateAppBar loggedIn={loggedIn} />

      <Card sx={{ p: 10 }}>
        <h1>Welcome to fullstack library app!</h1>
      </Card>
    </div>
    )
}