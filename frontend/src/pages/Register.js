import './Home.css';
import './Register.css';
import { useState } from 'react';
import { CreateAppBar } from '../components/NavBar';
import Card from "@mui/material/Card";
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Alert from '@mui/material/Alert';
import axios from 'axios';


const baseAPIUrl = 'http://localhost:8000';
const client = axios.create({
    baseURL: baseAPIUrl
})


export function RegisterForm({ loggedIn }) {
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState([]);
    const [messages, setMessages] = useState([]);

    function SubmitRegistration(e) {
        e.preventDefault();

        const data = {
            'email': email,
            'username': username,
            'first_name': firstName,
            'last_name': lastName,
            'password': password
        }
        client.post('/auth/register/', data)
        .then(response => {
            if (response.status === 201) {
                setMessages(response.data['messages']);
                setErrors([]);
            }
        })
        .catch(errors => {
            setErrors(errors.response.data['errors']);
            setMessages([]);
        });

        };

    return (
        <div className='Main-section'>
        <CreateAppBar loggedIn={loggedIn} />
        <Card sx={{ p: 10 }}>
            <Typography sx={{ mb: 3 }} variant='h2'>
                Registration
            </Typography>

            { errors.map((error) => (
                <Alert sx={{ mb: 4 }} severity='error'>{ error }</Alert>
            )) }

            { messages.map((message) => (
                <Alert sx={{ mb: 4 }} severity='success'>{ message }</Alert>
            )) }

            <form onSubmit={SubmitRegistration}>
                <Stack direction={"column"} sx={{ width: '25vw' }}>
                
                    <TextField
                        required
                        id="outlined-required"
                        label="Email"
                        sx={{ mb: 4 }}
                        onChange={event => setEmail(event.target.value)}
                        />


                    <TextField
                        required
                        id="outlined-required"
                        label="Username"
                        sx={{ mb: 4 }}
                        onChange={event => setUsername(event.target.value)}
                        />
                        
                    <TextField
                        required
                        id="outlined-required"
                        label="First name"
                        sx={{ mb: 4 }}
                        onChange={event => setFirstName(event.target.value)}
                        />

                    <TextField
                        required
                        id="outlined-required"
                        label="Last name"
                        sx={{ mb: 4 }}
                        onChange={event => setLastName(event.target.value)}
                        />

                    <TextField
                        required
                        id="outlined-required"
                        label="Password"
                        type='password'
                        sx={{ mb: 4 }}
                        onChange={event => setPassword(event.target.value)}
                        />

                    <Button size='medium' variant='contained' type='submit'>Create accout</Button>


                </Stack>
            </form>
        </Card>
        </div>
    )   
}