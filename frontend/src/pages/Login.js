import './Home.css';
import './Register.css';
import { useState } from 'react';
import { CreateAppBar } from '../components/NavBar';
import Card from "@mui/material/Card";
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Alert from '@mui/material/Alert';
import { redirect } from 'react-router-dom';
import axios from 'axios';


export const baseAPIUrl = 'http://localhost:8000';
export const client = axios.create({
    baseURL: baseAPIUrl
})


export function LoginForm({ loggedIn }) {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loginErrors, setErrors] = useState([]);
    const [token, setToken] = useState('');

    function SubmitLogin(e) {
        e.preventDefault();

        const data = {
            'username': username,
            'password': password
        }

        client.post('/auth/login/', data)
        .then(response => {
            if (response.status === 200) {
                setToken(response.data['token']);
                loggedIn = true;
                setErrors([]);
            }
        })
        .catch(errors => {
            setErrors(errors.response.data['errors']);
        });

        localStorage.setItem("token", token);
        console.log(token);

        if (loggedIn) {
            redirect("/books/");
        }
        
    };

    
    

    return (
        <div className='Main-section'>
        <CreateAppBar loggedIn={loggedIn} />
        <Card sx={{ p: 10 }}>
            <Typography sx={{ mb: 3 }} variant='h2'>
                Login
            </Typography>

            { loginErrors.map((error) => (
                <Alert sx={{ mb: 4 }} severity='error'>{ error }</Alert>
            )) }

            <form onSubmit={SubmitLogin}>
                <Stack direction={"column"} sx={{ width: '25vw' }}>

                    <TextField
                        required
                        id="outlined-required"
                        label="Username"
                        sx={{ mb: 4 }}
                        onChange={event => setUsername(event.target.value)}
                        />
                        
                    <TextField
                        required
                        id="outlined-required"
                        label="Password"
                        type='password'
                        sx={{ mb: 4 }}
                        onChange={event => setPassword(event.target.value)}
                        />

                    <Button size='large' variant='contained' type='submit'>Login</Button>

                </Stack>
            </form>
        </Card>
        </div>
    )   
}