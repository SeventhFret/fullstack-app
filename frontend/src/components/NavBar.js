import book from '../assets/book.svg'
import Button from '@mui/material/Button';
import AppBar from '@mui/material/AppBar';
import { Icon, Toolbar, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

export function CreateAppBar(props) {

    if (props.loggedIn) {
        return (
        <AppBar color='secondary'>
            <Toolbar>
                <Icon>
                    <img src={book} alt='book logo' height={25} width={25} />
                </Icon>        

                <Link to='/' style={{ textDecoration: 'none' }}>
                    <Typography variant='h5' component='a' sx={{ mx: 2, color: 'white', textDecoration: 'none' }}>Library</Typography>
                </Link>

                <Link to="/logout/" style={{ textDecoration: 'none' }}>
                    <Button variant='contained' size='medium'>Logout</Button>
                </Link>
                
                <Link to='/books/' style={{ textDecoration: 'none' }}>
                    <Button sx={{ m: 2 }} variant='contained' size='medium' href='registration/'>Books</Button>
                </Link>

            </Toolbar>
        </AppBar>
        )
    } else {

        return (
            <AppBar color='secondary'>
            <Toolbar>
                <Icon>
                    <img src={book} alt='book logo' height={25} width={25} />
                </Icon>        

                <Link to='/' style={{ textDecoration: 'none' }}>
                    <Typography variant='h5' component='a' sx={{ mx: 2, color: 'white', textDecoration: 'none' }}>Library</Typography>
                </Link>

                <Link to="/login/" style={{ textDecoration: 'none' }}>
                    <Button variant='contained' size='medium'>Login</Button>
                </Link>
                
                <Link to='/registration/' style={{ textDecoration: 'none' }}>
                    <Button sx={{ m: 2 }} variant='contained' size='medium' href='registration/'>Sign Up</Button>
                </Link>

            </Toolbar>
        </AppBar>
    )
}
}